{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

module Geometry where
import Prelude hiding (lines)
import Data.List hiding (lines)
import Control.Monad
import Debug.Trace
import qualified Data.Set as Set
import Data.Number.IReal.Powers
import Control.DeepSeq (NFData(..))

import Approx

data Point =
  Point
    { xCoord :: {-# UNPACK #-}!R
    , yCoord :: {-# UNPACK #-}!R
    }
  deriving (Show, Eq, Ord)

instance NFData Point where
  rnf (Point x y) = rnf x `seq` rnf y

data Slope = Inf | Fin {-# UNPACK #-}!R-- TODO: think about strictness
  deriving (Show)

instance Eq Slope where
  {-# INLINE (==) #-}
  Inf == Inf = True
  (Fin x) == (Fin y)  = x == y
  _ == _ = False

instance Ord Slope where
  {-# INLINE compare #-}
  compare (Fin x) (Fin y)  = compare x y
  compare Inf Inf = EQ
  compare Inf _ = GT
  compare _ Inf = LT

data Line =
  Line
    { m :: !Slope
    , c :: {-# UNPACK #-}!R
    }
  deriving (Show)

instance Eq Line where
  {-# INLINE (==) #-}
  x == y = m x == m y && c x == c y

instance Ord Line where
  {-# INLINE compare #-}
  -- NB: first compare 'm' as otherwise groupSlopes will be wrong
  compare x y = compare (m x) (m y) <> compare (c x) (c y)

-- | (x-a)^2 + (y-b)^2
data Circle =
  Circle
  { a :: {-# UNPACK #-}!R
  , b :: {-# UNPACK #-}!R
  , r :: {-# UNPACK #-}!R
  , rsq :: {-# UNPACK #-}!R -- ^ rsq = r^2
  }
  deriving (Show)

instance Eq Circle where
  {-# INLINE (==) #-}
  c1 == c2 = rsq c1 == rsq c2 && a c1 == a c2 && b c1 == b c2

instance Ord Circle where
  {-# INLINE compare #-}
  compare c1 c2 = compare (rsq c1) (rsq c2) <> compare (a c1) (a c2) <> compare (b c1) (b c2)

pointOnLine :: Point -> Line -> Bool
pointOnLine (Point x _) (Line Inf c) = c == x
pointOnLine (Point x y) (Line (Fin m) c) = y == m*x + c

intersectLL :: Line -> Line -> [Point]
intersectLL (Line Inf _ ) (Line Inf _ ) = []
intersectLL (Line Inf x1) (Line (Fin m2) c2) = [Point x1 (x1*m2+c2)]--(x1*ma2 + c2*mb2)/mb2)]
intersectLL (Line (Fin m1 ) c1) (Line Inf x2) = [Point x2 (x2*m1+c1)]--(x2*ma1 + c1*mb1)/mb1)]
intersectLL (Line (Fin m1 ) c1) (Line (Fin m2) c2)
  | m1 /= m2 = [Point x y]
  | otherwise = []
  where
    x = (c2 - c1)/(m1 - m2)
    y = x*m1 + c1
    --y = (m1*c2 - m2*c1)/(m1 - m2)

intersectLL1 :: Line -> Line -> [Point]
intersectLL1 (Line Inf _) (Line Inf _) = []
intersectLL1 (Line Inf x1) (Line (Fin m2) c2) = [Point x1 (x1*m2+c2)]
intersectLL1 (Line (Fin m1) c1) (Line Inf x2) = [Point x2 (x2*m1+c1)]
intersectLL1 (Line (Fin m1) c1) (Line (Fin m2) c2)
   = [Point x y]
  where
    x = (c2 - c1)/(m1 - m2)
    y = (m1*c2 - m2*c1)/(m1 - m2)

-- 1.485675599731654e7 is maximum manhattan distance divided by 2 computed in 964 seconds
-- maximum distance between any two points at stage two is 18

-- | 'distLinePointSqrd' gives the square of the minimum distance between a line and a point
distLinePointSqrd :: Line -> Point -> Point
{-# INLINE distLinePointSqrd #-}
distLinePointSqrd (Line Inf c) (Point a _)  = Point (sq (a-c)) 1
distLinePointSqrd (Line (Fin m) c) (Point a b)  = Point (sq (m*a - b + c)) (sq m + 1)--(c^2 + y^2 + (m^2)*(x^2) + 2*x*m (c - y) - 2*y*c, (m^2 + 1))

pointOnCircle :: Point -> Circle -> Bool
pointOnCircle (Point x y) Circle{..} = sq (x-a) + sq (y-b) == rsq

intersectCL :: Circle -> Line -> [Point]
intersectCL Circle{..} l@Line{..}
  | rt < 0 = []
  | otherwise = case m of
      Inf -> [Point c (yInf 1), Point c (yInf (-1))]
      Fin m ->
        if rt == 0 then
          [Point (x m 0) (y m 0)]
        else
          [Point (x m 1) (y m 1), Point (x m (-1)) (y m (-1))]
  where
    (Point cda cdb) = distLinePointSqrd l (Point a b)
    rt =  rsq*cdb - cda
    yInf s = b + s*sqrt rt
    x m s = (s*sqrt rt + a + m*(b-c))/cdb
    y m s = x m s*m + c

intersectCC :: Circle -> Circle -> [Point]
intersectCC (Circle x1 y1 r1 rsq1) (Circle x2 y2 r2 rsq2)
         | hi < 0 = []
         | comp == GT = []
         | comp == EQ = [Point sx sy]
         | otherwise =  [Point (x4 1) (y4 1), Point (x4 (-1)) (y4 (-1))]
  where
    difx = x2 - x1
    dify = y2 - y1
    d = sq difx + sq dify
    sd = sqrt d
    rsum = r1 + r2
    comp = compare sd rsum
    sx = (r2*x1 + r1*x2)/rsum
    sy = (r2*y1 + r1*y2)/rsum
    an = rsq1 - rsq2 + d
    hi  = rsq1*4*d - sq an
    h =  sqrt hi
    --x3 = x1 + (an*difx)/(2*d)
    --y3 = y1 + (an*dify)/(2*d)
    x4 s = x1 + (an*difx + s*h*dify)/(2*d)
    y4 s = y1 + (an*dify - s*h*difx)/(2*d)


line :: Point -> Point -> Line
line (Point x1 y1) (Point x2 y2)
  | x2 == x1 = Line Inf x1
  | otherwise = Line (Fin m) c
  where
    ma = y2 - y1
    mb = x2 - x1
    m = ma/mb
    c = y1 - m*x1 -- (y1*mb - x1*ma)/mb -- this is slow for construct but fast for floats
    --c = (y1*mb - x1*ma)/mb -- this is slow for construct but fast for floats

circle :: Point -> Point -> Circle
circle (Point x1 y1) (Point x2 y2) = Circle x1 y1 r rsq
  where
    r = sqrt rsq
    rsq = sq (x2 - x1) + sq (y2 - y1)

fnub :: Ord a => [a] -> [a]
fnub = Set.toList . Set.fromList

-- | 'lines xs' is the set of distinct lines that can be drawn through any two points from 'xs'
lines :: [Point] -> [Line]
lines xs = fnub [line x y | x <- xs, y <- xs, x /= y]

groupSlope :: [Line] -> [[Line]]
groupSlope = groupBy (\x y -> m x == m y) . sort

intsLL :: [[Line]] -> [Point]
intsLL [] = []
intsLL (l:ls) = l >>= \l1 -> (ls >>= (\l2 -> l2 >>= intersectLL1 l1)) ++ intsLL ls

intsLL1 :: [[Line]] -> [[Point]]
intsLL1 [] = []
intsLL1 (l:ls) = every 1000 0 0 (l >>= \l1 -> ls >>= (\l2 -> l2 >>= intersectLL1 l1)):intsLL1 ls

circles :: [Point] -> [Circle]
circles xs = fnub $ circles' xs []
  where
    --circles' :: [Point Number] -> [Point Number] -> [Circle Number]
    circles' [] _ = []
    circles' (x:xs) ys = fmap (circle x) (xs++ys) ++ circles' xs (x:ys)


points :: [Point] -> [Point]
points ps = fnub $ ill ++ ilc ++ icc
  where
    ls = lines ps
    cs = circles ps
    ccs [] = []
    ccs (x:xs) = (filter (difCenter x) xs >>= intersectCC x) ++ ccs xs
    difCenter c1 c2 = a c1 /= a c2 || b c1 /= b c2
    ill = intsLL $ groupSlope ls--join $ liftM2 intersectLL ls ls
    ilc = join $ liftM2 intersectCL cs ls
    icc = ccs cs

every :: Int -> Int -> Int -> [c] -> [c]
every _ _ _ [] = []
every n 0 z xs = trace ("BEEP:" ++ show z) $ every n n z xs
every n m z (x:xs) = x:every n (m-1) (z+1) xs


start :: [Point]
start = [Point (-0.5) 0, Point 0.5 0]

-- | 'fq' gives True for a point in the first quadrant (positive in both x and y)
fq :: Point -> Bool
fq (Point x y) = x>=0 && y>=0

stripPoint :: Point -> [R]
stripPoint (Point a b) = [a, b]

stripLine :: Line -> [R]
stripLine (Line Inf c) = [c]
stripLine (Line (Fin m) c) = [m, c]

stripCircle :: Circle -> [R]
stripCircle Circle{..} = [a, b, r, rsq]

strip :: [Point] -> [R]
strip ps = fnub $ (ps >>= stripPoint) ++ (lines ps >>= stripLine) ++ (circles ps >>= stripCircle)

minDiff :: [R] -> R
minDiff xs = minimum $ map abs $ zipWith (-) xs' (tail xs')
  where xs' = filter (>0) xs
