import Test.Tasty.Bench

import Geometry

main :: IO ()
main = defaultMain
  [ bgroup "Calculating the <=2 constructibility numbers"
    [ bench "first" $ nf points  start
    , bench "second" $ nf (points . points) start
    , bench "second and a bit" $ nf (points . take 10 . points . points) start
    ]
  ]
