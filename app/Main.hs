module Main where
import Prelude hiding (lines)
import Data.Real.Constructible
import Data.List (intercalate)

import Geometry
import Sweep
import Approx

res :: [Point] -> Result
res ps = snd $ runSweep $ go obs
  where
    obs = ls ++ cs
    mapCircle f (Circle a b c d) = Circle (f a) (f b) (f c) (f d)
    mapLine f (Line a b) = Line (mapSlope f a) (f b)
    mapSlope f Inf = Inf
    mapSlope f (Fin x) = Fin $ f x
    ls = fmap L $ mapLine reApprox <$> lines ps
    cs = (mapCircle reApprox <$> circles ps) >>= (\c -> [C c True, C c False])

main :: IO ()
main = putStrLn $ intercalate ", " $ [show leftB, show rightB, show a, show b, show c, show d , show (d*4 - 2*(b+c) + a)]
  where
    r@(a,b,c,d) = res $ points $ points $ start
